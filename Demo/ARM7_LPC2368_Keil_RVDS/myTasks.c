#include <stdlib.h>

/* Scheduler include files. */
#include "FreeRTOS.h"
#include "task.h"

/* le fichier .h du TP  */
#include "myTasks.h"

#define ledSTACK_SIZE		configMINIMAL_STACK_SIZE
#define ledNUMBER_OF_LEDS	( 7 )

/* The task that is created three times. */
static portTASK_FUNCTION_PROTO( vIHM_boutons, pvParameters );
//static portTASK_FUNCTION_PROTO( vLed2Task, pvParameters );
//static portTASK_FUNCTION_PROTO( vLed3Task, pvParameters );
//static portTASK_FUNCTION_PROTO( vLed4Task, pvParameters );
/*-----------------------------------------------------------*/

void vInit_myTasks( UBaseType_t uxPriority )
{
//BaseType_t xLEDTask;
		xTaskCreate( vIHM_boutons, "IHMB", ledSTACK_SIZE, NULL, uxPriority, ( TaskHandle_t * ) NULL );
//	  xTaskCreate( vLed2Task, "LED2", ledSTACK_SIZE, NULL, uxPriority, ( TaskHandle_t * ) NULL );
//	  xTaskCreate( vLed3Task, "LED3", ledSTACK_SIZE, NULL, uxPriority, ( TaskHandle_t * ) NULL );
//	  xTaskCreate( vLed4Task, "LED4", ledSTACK_SIZE, NULL, uxPriority, ( TaskHandle_t * ) NULL );
}
/*-----------------------------------------------------------*/

static portTASK_FUNCTION( vIHM_boutons, pvParameters )
{
	unsigned char old_bouton = 1, bouton = 0;
	unsigned char status_rs232_0;
	unsigned char cpt_appui_bp_0 = 48;
	/* Les parametres ne sont pas utilis�s. */
	( void ) pvParameters;
  
	for(;;){
		vTaskDelay(33);
		//lire P2.10
		old_bouton = bouton;
		bouton = (LPC_GPIO2 -> FIOPIN & (1<<10)) ?1:0;
		if(!bouton & old_bouton){
			status_rs232_0 = LPC_UART0->LSR;
			if(status_rs232_0 & 1<<5) { LPC_UART0->THR= ++cpt_appui_bp_0;}
		}
		
	}
	if(cpt_appui_bp_0 >= 127) cpt_appui_bp_0 = 48;
} /*lint !e715 !e818 !e830 Function definition must be standard for task creation. */


static portTASK_FUNCTION( vLed2Task, pvParameters )
{
	/* Les parametres ne sont pas utilis�s. */
	( void ) pvParameters;
    LPC_GPIO3->FIOCLR = 1<<26;
		LPC_GPIO0->FIOCLR = 1<<22;
    LPC_GPIO3->FIOCLR = 1<<25;	
	for(;;)
	{ LPC_GPIO3->FIOCLR = 1<<26;
		LPC_GPIO0->FIOSET = 1<<22;
    LPC_GPIO3->FIOCLR = 1<<25;	
	}
} /*lint !e715 !e818 !e830 Function definition must be standard for task creation. */
static portTASK_FUNCTION( vLed3Task, pvParameters )
{
	/* Les parametres ne sont pas utilis�s. */
	( void ) pvParameters;
    LPC_GPIO3->FIOCLR = 1<<26;
		LPC_GPIO0->FIOCLR = 1<<22;
    LPC_GPIO3->FIOCLR = 1<<25;	  
	for(;;)
	{
    LPC_GPIO3->FIOCLR = 1<<26;
		LPC_GPIO0->FIOCLR = 1<<22;
    LPC_GPIO3->FIOSET = 1<<25;	
	}
} /*lint !e715 !e818 !e830 Function definition must be standard for task creation. */

static portTASK_FUNCTION( vLed4Task, pvParameters )
{
	/* Les parametres ne sont pas utilis�s. */
	( void ) pvParameters;
    LPC_GPIO3->FIOCLR = 1<<26;
		LPC_GPIO0->FIOCLR = 1<<22;
    LPC_GPIO3->FIOCLR = 1<<25;
	for(;;)
	{
    LPC_GPIO3->FIOSET = 1<<26;
		LPC_GPIO0->FIOSET = 1<<22;
    LPC_GPIO3->FIOSET = 1<<25;
	}
} /*lint !e715 !e818 !e830 Function definition must be standard for task creation. */

